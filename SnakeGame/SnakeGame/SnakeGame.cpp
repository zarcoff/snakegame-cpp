#include <iostream>
#include <list>
#include <thread>
#define _WIN32_WINNT 0x0500
#include <windows.h>
HWND consoleWindow = GetConsoleWindow();
using namespace std;


int nScreenWidth = 120;
int nScreenHeight = 30;

struct sSnakeSegment
{
	int x;
	int y;
};


int main()

{	// Lock Window Size
	SetWindowLong(consoleWindow, GWL_STYLE, GetWindowLong(consoleWindow, GWL_STYLE) & ~WS_MAXIMIZEBOX & ~WS_SIZEBOX);

	// Create Screen Buffer
	wchar_t* screen = new wchar_t[nScreenWidth * nScreenHeight];
	for (int i = 0; i < nScreenWidth * nScreenHeight; i++) screen[i] = L' ';
	HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	SetConsoleActiveScreenBuffer(hConsole);
	DWORD dwBytesWritten = 0;

	// Setup for r/w to file
	FILE* f;


	while (1)
	{
		// Variables Setup
		list<sSnakeSegment> snake = { {60,15},{61,15},{62,15},{63,15},{64,15},{65,15},{66,15},{67,15},{68,15},{69,15} };
		int nFoodX = 30;
		int nFoodY = 15;
		int nScore = 0;
		int nHighScore = 0;
		int nSnakeDirection = 3;
		bool isDead = false;
		bool bKeyLeft = false, bKeyRight = false, bKeyUp = false, bKeyDown = false, bKeyLeftOld = false, bKeyRightOld = false, bKeyUpOld = false, bKeyDownOld = false;


		// Read HighScore from file
		fopen_s(&f, "properties.snake", "r");
		fscanf_s(f, "%d", &nHighScore);
		fclose(f);

		// Wait for 'SPACE'
		wsprintf(&screen[nScreenHeight * nScreenWidth / 2 + 48], L"PRESS 'SPACE' TO START");
		WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);
		while ((0x8000 & GetAsyncKeyState((unsigned char)('\x20'))) == false);

		while (!isDead)
		{
			// Frame Timing, compensate for aspect ratio of CMD
			auto t1 = chrono::system_clock::now();
			while ((chrono::system_clock::now() - t1) < ((nSnakeDirection % 2 == 1) ? 60ms : 60ms))
			{
				// Get Input
				bKeyRight = (0x8000 & GetAsyncKeyState((unsigned char)('\x27'))) != false;
				bKeyLeft = (0x8000 & GetAsyncKeyState((unsigned char)('\x25'))) != false;
				bKeyUp = (0x8000 & GetAsyncKeyState((unsigned char)('\x26'))) != false;
				bKeyDown = (0x8000 & GetAsyncKeyState((unsigned char)('\x28'))) != false;

				if (bKeyRight && !bKeyRightOld && nSnakeDirection != 3)
				{
					nSnakeDirection = 1;
				}

				if (bKeyLeft && !bKeyLeftOld && nSnakeDirection != 1)
				{
					nSnakeDirection = 3;
				}

				if (bKeyUp && !bKeyUpOld && nSnakeDirection != 2)
				{
					nSnakeDirection = 0;
				}

				if (bKeyDown && !bKeyDownOld && nSnakeDirection != 0)
				{
					nSnakeDirection = 2;
				}

				bKeyRightOld = bKeyRight;
				bKeyLeftOld = bKeyLeft;
				bKeyUpOld = bKeyUp;
				bKeyDownOld = bKeyDown;
			}

			// ==== Logic

			// Update Snake Position, place a new head at the front of the list in
			// the right direction
			switch (nSnakeDirection)
			{
			case 0: // UP
				snake.push_front({ snake.front().x, snake.front().y - 1 });
				break;
			case 1: // RIGHT
				snake.push_front({ snake.front().x + 1, snake.front().y });
				break;
			case 2: // DOWN
				snake.push_front({ snake.front().x, snake.front().y + 1 });
				break;
			case 3: // LEFT
				snake.push_front({ snake.front().x - 1, snake.front().y });
				break;
			}

			// Collision Detection Snake /w Food
			if (snake.front().x == nFoodX && snake.front().y == nFoodY)
			{
				nScore++;
				if (nScore > nHighScore)
				{
					nHighScore++;
				}
				while (screen[nFoodY * nScreenWidth + nFoodX] != L' ')
				{
					nFoodX = rand() % nScreenWidth;
					nFoodY = (rand() % (nScreenHeight - 3)) + 3;
				}

				for (int i = 0; i < 5; i++)
					snake.push_back({ snake.back().x, snake.back().y });
			}

			// Collision Detect Snake /w World
			if (snake.front().x <= 0 || snake.front().x >= nScreenWidth)
				isDead = true;
			if (snake.front().y <= 3 || snake.front().y >= nScreenHeight)
				isDead = true;

			// Collision Detect Snake /w Snake
			for (list<sSnakeSegment>::iterator i = snake.begin(); i != snake.end(); i++)
				if (i != snake.begin() && i->x == snake.front().x && i->y == snake.front().y)
					isDead = true;

			// Chop off Snake's tail 
			snake.pop_back();

			// ==== Presentation

			// Clear Screen
			for (int i = 0; i < nScreenWidth * nScreenHeight; i++) screen[i] = L' ';

			// Draw Stats & Border
			for (int i = 0; i < nScreenWidth; i++)
			{
				screen[i] = L'=';
				screen[2 * nScreenWidth + i] = L'=';
			}
			wsprintf(&screen[165], L"SCORE: %d            HIGHSCORE: %d", nScore, nHighScore);

			// Draw Snake's Body
			for (auto s : snake)
				screen[s.y * nScreenWidth + s.x] = isDead ? L'+' : L'o';

			// Draw Snake's Head
			screen[snake.front().y * nScreenWidth + snake.front().x] = isDead ? L'X' : L'@';

			// Draw Food
			screen[nFoodY * nScreenWidth + nFoodX] = L'%';

			if (isDead)
				wsprintf(&screen[15 * nScreenWidth + 40], L"    PRESS 'SPACE' TO PLAY AGAIN    ");

			// Display Frame
			WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);
		}
		if (nScore == nHighScore)
		{
			fopen_s(&f, "properties.snake", "w");
			fprintf(f, "%d", nScore);
			fclose(f);
		}


		// Wait for space
		while ((0x8000 & GetAsyncKeyState((unsigned char)('\x20'))) == false);
	}

	return 0;
}

